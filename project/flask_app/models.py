from datetime import datetime as dt
from typing import Optional

from flask_app.app import db
from sqlalchemy import and_


class Client(db.Model):
    __tablename__ = "clients"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    surname = db.Column(db.String(50), nullable=False)
    credit_card = db.Column(db.String(50))
    car_number = db.Column(db.String(10))
    clients_parking = db.relationship(
        "ClientParking", backref="client", lazy=True
    )

    def add(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def get(cls, pk: int) -> Optional["Client"]:
        return db.session.query(cls).filter(cls.id == pk).first()


class Parking(db.Model):
    __tablename__ = "parkings"

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(100), nullable=False)
    opened = db.Column(db.Boolean)
    count_places = db.Column(db.Integer, nullable=False)
    count_available_places = db.Column(db.Integer, nullable=False)
    parking_clients = db.relationship(
        "ClientParking", backref="parking", lazy=True
    )

    def add(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def get(cls, pk: int) -> Optional["Parking"]:
        return db.session.query(cls).filter(cls.id == pk).first()


class ClientParking(db.Model):
    __tablename__ = "client_parking"

    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.Integer, db.ForeignKey("clients.id"))
    parking_id = db.Column(db.Integer, db.ForeignKey("parkings.id"))
    time_in = db.Column(db.DateTime)
    time_out = db.Column(db.DateTime)

    @classmethod
    def get_not_ended(
        cls, client_id: int, parking_id: int
    ) -> Optional["ClientParking"]:
        return (
            db.session.query(ClientParking)
            .where(
                and_(
                    ClientParking.client_id == client_id,
                    ClientParking.parking_id == parking_id,
                    ClientParking.time_out.is_(None),
                )
            )
            .first()
        )

    def get_in(self) -> bool:
        saved = False
        with db.session.begin_nested():
            self.time_in = dt.now()
            db.session.add(self)
            parking = Parking.get(self.parking_id)
            if not parking:
                raise Exception("Parking not found")
            parking.count_available_places -= 1
            saved = True
        return saved

    def get_out(self) -> bool:
        saved = False
        with db.session.begin_nested():
            self.time_out = dt.now()
            parking = Parking.get(self.parking_id)
            if not parking:
                raise Exception("Parking not found")
            parking.count_available_places += 1
            saved = True
        db.session.commit()
        return saved
