from flask import Flask
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
maw = Marshmallow()


def init_app() -> Flask:
    app = Flask("my_site")
    app.config["DEBUG"] = True
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///database.db"

    db.init_app(app)
    maw.init_app(app)

    with app.app_context():
        from flask_app import models  # noqa

        db.create_all()

    from flask_app.routes import main_bp

    app.register_blueprint(main_bp)

    return app
