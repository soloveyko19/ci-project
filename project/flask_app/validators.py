from flask_app.models import Client, Parking
from marshmallow.validate import ValidationError


def client_validate(client_id: int):
    client = Client.get(client_id)
    if not client:
        raise ValidationError("client with current id does not exist")
    if not client.credit_card:
        raise ValidationError("no payment method provided")


def parking_validate(parking_id: int):
    parking = Parking.get(parking_id)

    if not parking:
        raise ValidationError("parking with current id does not exist")
    if parking.count_available_places == 0:
        raise ValidationError("parking does not has available places")
    if not parking.opened:
        raise ValidationError("parking is closed")
