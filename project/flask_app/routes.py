from flask import Blueprint, jsonify, request
from flask_app.app import db
from flask_app.models import Client, ClientParking, Parking
from flask_app.schemas import ClientParkingSchema, ClientSchema, ParkingSchema
from marshmallow import ValidationError

main_bp = Blueprint("main", __name__)


@main_bp.get("/clients")
def get_clients_list():
    schema = ClientSchema(many=True)
    clients = db.session.query(Client).all()
    return jsonify(clients=schema.dump(clients))


@main_bp.post("/clients")
def post_clients_create():
    schema = ClientSchema()
    data = request.get_json()
    if not data:
        return jsonify(error="No data provided"), 400
    try:
        valid_data = schema.load(data)
    except ValidationError as e:
        return jsonify(error=e.messages), 400
    client = Client(**valid_data)
    client.add()
    return jsonify(schema.dump(client)), 201


@main_bp.get("/clients/<int:pk>")
def get_clients_detail(pk: int):
    schema = ClientSchema()
    client = Client.get(pk=pk)
    if not client:
        return jsonify(error="No client with current id found"), 404
    return jsonify(schema.dump(client))


@main_bp.post("/parkings")
def post_parkings_create():
    schema = ParkingSchema()
    data = request.json
    if not data:
        return jsonify(error="No data provided"), 400
    try:
        valid_data = schema.load(data)
    except ValidationError as e:
        return jsonify(error=e.messages), 400
    parking = Parking(**valid_data)
    parking.add()
    return jsonify(schema.dump(parking)), 201


@main_bp.post("/client-parkings")
def post_client_parkings_add():
    schema = ClientParkingSchema()
    data = request.json
    if not data:
        return jsonify(error="No data provided"), 400
    try:
        valid_data = schema.load(data)
    except ValidationError as e:
        return jsonify(error=e.messages), 400
    client_parking = ClientParking(
        client_id=valid_data.get("client_id"),
        parking_id=valid_data.get("parking_id"),
    )
    saved = client_parking.get_in()
    if not saved:
        return jsonify(error="Error in transaction"), 400
    return jsonify(message="Accepted")


@main_bp.delete("/client-parkings")
def delete_client_parkings_remove():
    schema = ClientParkingSchema()
    data = request.json
    if not data:
        return jsonify(error="No data provided"), 400
    try:
        valid_data = schema.load(data)
    except ValidationError as e:
        return jsonify(error=e.messages), 400
    client_parking = ClientParking.get_not_ended(**valid_data)
    if not client_parking:
        return jsonify(error="Incorrect data"), 400
    saved = client_parking.get_out()
    if not saved:
        return jsonify(error="Error in transaction"), 400
    return jsonify(message="Accepted")
