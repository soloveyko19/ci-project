from flask_app.app import maw
from flask_app.models import Client, Parking
from flask_app.validators import client_validate, parking_validate
from marshmallow import Schema, fields


class ClientSchema(maw.SQLAlchemyAutoSchema):
    class Meta:
        model = Client


class ParkingSchema(maw.SQLAlchemyAutoSchema):
    class Meta:
        model = Parking


class ClientParkingSchema(Schema):
    class Meta:
        fields = ("client_id", "parking_id")

    client_id = fields.Integer(required=True, validate=[client_validate])
    parking_id = fields.Integer(required=True, validate=[parking_validate])
