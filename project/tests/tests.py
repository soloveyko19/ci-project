import pytest
from flask_app.models import ClientParking, Parking
from tests.factories import ClientFactory, ParkingFactory


@pytest.mark.parametrize("route", ["/clients", "/clients/1"])
def test_all_get_requests(client_fixture, route):
    client = client_fixture
    response = client.get(route)
    assert response.status_code == 200


def test_create_client(client_fixture):
    client = client_fixture
    data = {
        "name": "name",
        "surname": "surname",
        "car_number": "123asd123",
        "credit_card": "1234-5678-9012-3456",
    }
    response = client.post("/clients", json=data)
    assert response.status_code == 201
    response_data = response.get_json()
    for key, value in data.items():
        assert response_data.get(key) == value


def test_create_parking(client_fixture):
    client = client_fixture
    data = {
        "address": "address",
        "opened": True,
        "count_places": 400,
        "count_available_places": 390,
    }
    response = client.post("/parkings", json=data)
    assert response.status_code == 201
    response_data = response.get_json()
    for key, value in data.items():
        assert response_data.get(key) == value


@pytest.mark.parking
def test_entry_to_parking(client_fixture):
    client = client_fixture
    data = {"client_id": 1, "parking_id": 1}

    parking = Parking.get(1)
    parking_prev_places_count = parking.count_available_places

    response = client.post("/client-parkings", json=data)
    assert response.status_code == 200

    client_parking = ClientParking.get_not_ended(**data)
    assert client_parking

    parking = Parking.get(1)
    parking_new_places_count = parking.count_available_places
    assert parking_new_places_count + 1 == parking_prev_places_count


@pytest.mark.parking
def test_exit_from_parking(client_fixture):
    client = client_fixture
    data = {"client_id": 1, "parking_id": 1}

    client_parking = ClientParking(**data)
    client_parking.get_in()

    parking = Parking.get(1)
    parking_prev_places_count = parking.count_available_places

    response = client.delete("/client-parkings", json=data)
    assert response.status_code == 200

    client_parking = ClientParking.get_not_ended(**data)
    assert not client_parking

    parking = Parking.get(1)
    parking_new_places_count = parking.count_available_places
    assert parking_prev_places_count + 1 == parking_new_places_count


# Duplicates


def test_create_client_duplicate(db_fixture):
    db = db_fixture
    client = ClientFactory()
    db.session.commit()
    assert client.id is not None


def test_create_parking_duplicate(db_fixture):
    db = db_fixture
    parking = ParkingFactory()
    db.session.commit()
    assert parking.id is not None
