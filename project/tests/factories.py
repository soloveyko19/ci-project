import random
from typing import Optional

import factory
from factory import fuzzy
from flask_app.app import db
from flask_app.models import Client, Parking


class ClientFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name: factory.Faker = factory.Faker("first_name")
    surname: factory.Faker = factory.Faker("last_name")
    credit_card: Optional[factory.Faker] = (
        factory.Faker("credit_card_number")
        if random.choice([True, False])
        else None
    )
    car_number = fuzzy.FuzzyText(length=10)


class ParkingFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address: factory.Faker = factory.Faker("address")
    opened: factory.Faker = factory.Faker("boolean")
    count_places: factory.Faker = factory.Faker(
        "pyint", min_value=10, max_value=500
    )
    count_available_places: factory.LazyAttribute = factory.LazyAttribute(
        lambda parking: random.randint(10, parking.count_places)
    )
