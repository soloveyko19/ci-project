import pytest
from flask_app.app import db, init_app
from flask_app.models import Client, ClientParking, Parking


@pytest.fixture()
def app_fixture():
    app = init_app()
    app.config["TESTING"] = True
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///test_database.db"

    with app.app_context():
        db.create_all()
        client = Client(
            name="name",
            surname="surname",
            car_number="123qwe123",
            credit_card="1234-1231-1432-3423",
        )
        client.add()
        parking = Parking(
            address="Sadovaya 123a, Oleksandria, Ukraine",
            opened=True,
            count_places=555,
            count_available_places=555,
        )
        parking.add()
        client_parking = ClientParking(
            client_id=client.id, parking_id=parking.id
        )
        client_parking.get_in()
        client_parking.get_out()

        yield app

        db.session.remove()
        db.drop_all()


@pytest.fixture()
def client_fixture(app_fixture):
    app = app_fixture
    client = app.test_client()
    yield client


@pytest.fixture()
def db_fixture(app_fixture):
    app = app_fixture
    with app.app_context():
        yield db
